class PopularProductsCell < Cell::Rails

  def display(args)
    @products=Spree::Product.most_ordered(:count => args[:count])
    render
  end

end
