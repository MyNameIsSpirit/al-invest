
class RandomProductsDisplayCell < Cell::Rails
  helper Spree::BaseHelper

  append_view_path "app/views"
  def display(args)
    @products= Spree::Product.get_random_products(args)
    render
  end

  def display_from_random_category(args={})
    @category=Spree::Category.get_random
    if @category.nil?
      random_products=Spree::Product.get_random_products
    else
      random_products=Spree::Product.get_random_from_category(:category_id => @category.id)
    end

    if !random_products.nil?
      @products=random_products.active.all
    else
      @products=Array.new
    end

    render
  end
end
