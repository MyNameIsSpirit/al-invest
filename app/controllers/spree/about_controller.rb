module Spree
  class AboutController < Spree::StoreController
    respond_to :html

    def index
      @about = Spree::About.first
    end
  end
end
