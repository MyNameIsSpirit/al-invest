class Spree::Admin::AboutController < Spree::Admin::ResourceController
  def index
    about = Spree::About.all
  end

  def edit
    @about_user = Spree::About.find(params[:id])
  end

  def update
    @about_user = Spree::About.find(params[:id])
    if @about_user.update_attributes(params[:about_user])
      flash[:notice] = "About us updated"
    else
      render("edit")
    end

    redirect_to(:action => 'index')
  end
end
