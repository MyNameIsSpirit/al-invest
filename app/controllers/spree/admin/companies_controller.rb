class Spree::Admin::CompaniesController < Spree::Admin::ResourceController
  def index
    companies = Spree::Company.all
  end

  def new
    #MwsApi.submitProductInfo
    @company = Spree::Company.new
    @company.translations.push(Spree::Company::Translation.new(:locale =>"es"))
    @company.translations.push(Spree::Company::Translation.new(:locale =>"en"))
  end

  def create
    @company = Spree::Company.new(params[:company])
    @company.save
    flash[:notice] = 'Company created'
    redirect_to(:action => 'index')
  end

  def update
    @company = Spree::Company.find(params[:id])
    if @company.update_attributes(params[:company])
      flash[:notice] = "Company updated"
    else
      render("edit")
    end

    redirect_to(:action => 'index')
  end

  def edit
    @company = Spree::Company.find(params[:id])
  end

  def delete
    @company = Spree::Company.find(params[:id])
  end

  def destroy
    Spree::Company.find(params[:id]).delete
    flash[:notice] = "Company deleted"
    redirect_to(:action => 'index')
  end
end
