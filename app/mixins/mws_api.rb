module MwsApi
  # possible :mws_status=>
    #:to_submit or nil => never sent to mws
    #:to_update => to resend basic info
    #:invalid_submission => invalid data to submit
    #:added => sent to mws but not verified
    #:submission_error => server error
    #:submitted_price => sent price but not verified
    #:submitted_inventory => sent quantity but not verified
    #:submitted_images => sent images but not verified
    #:active => listing full and active
  def self.mwlog
    @@mwlog ||= Logger.new("#{Rails.root}/log/mws.log")
  end

  def self.build_mws
    result = Mws.connect(
        merchant: 'A1KNCM8VQNJBX3',
        access: 'AKIAIHIZ254GPKL2QETQ',
        secret: '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'
    )
  end
  def self.submitProductInfo
    begin
      mwlog.info(Time.new.to_s+' starting submitProductInfo for mws')
      puts 'starting submitProductInfo for mws'
      mws=build_mws
      #obtener todos los productos a sincronizar con mws
      products_to_sync=Spree::Product.where('mws_status != ? and mws_registration = ?',:active,true).active.all
      #verificar los estados
      products_to_sync.each do |product|

        puts ':product => '+product.to_yaml
        begin
          mwlog.info(Time.new.to_s+' :checking product status for '+product.to_yaml)
          if product.mws_status == :to_submit
            puts ':to_submit submitting product'
            mwlog.info(Time.new.to_s+' :to_submit submitting product')
            product.mws_submit mws
          elsif product.mws_status == :added
            puts ':added submitting price'
            mwlog.info(Time.new.to_s+' :added submitting price')
            product.mws_submit_price mws
          elsif  product.mws_status == :submitted_price
            puts ':submitted_price submitting inventory'
            mwlog.info(Time.new.to_s+' :submitted_price submitting inventory')
            product.mws_submit_inventory mws
          elsif product.mws_status == :submitted_inventory
            puts ':mws_status submitting inventory'
            mwlog.info(Time.new.to_s+ ' :mws_status submitting inventory')
            product.mws_submit_images mws
          elsif product.mws_status == :submitted_images
            puts ':submitted_images verifying last step'
            mwlog.info(Time.new.to_s+' :submitted_images verifying last step')
            result=product.mws_check_request_status mws
            if result
              puts ':submitted_images marked as :active'
              mwlog.info(Time.new.to_s+' :submitted_images marked as :active')
              prod=Spree::Product.where(:id => product.id).first
              prod.update_attributes(:mws_status => :active ,:mws_request_id => nil )
            end
          end

        rescue => prod_ex
          puts 'exception with product '+prod_ex.to_s
          puts exception.backtrace
          mwlog.info(Time.new.to_s+' exception in procedure '+prod_ex.to_s)
          mwlog.info(exception.backtrace.to_s)
        end
      end
    rescue => ex
      puts 'exception in procedure '+ex.to_s
      puts exception.backtrace
      mwlog.info(Time.new.to_s+' exception in procedure '+ex.to_s)
      mwlog.info(exception.backtrace.to_s)
    end

    puts 'ending submitProductInfo for mws'
    mwlog.info(Time.new.to_s+' ending submitProductInfo for mws')
  end
  def self.syncInventory
    #TODO: Sync inventory
  end


  module Inventories
    def mws_update_inventory
      inventory=self
      puts "Inventory updated "
    end
  end
  module Products
    include MwsApiProducts::Products
  end


end