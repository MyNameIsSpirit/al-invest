module MwsApiProducts


  module Products


    def mwlog
      @@mwlog ||= Logger.new("#{Rails.root}/log/mws.log")
    end
    def mws_check_request_status(mws)
      request_id=self.mws_request_id
      #result=false
      if !request_id.nil?
        begin
          server_status = mws.feeds.get(request_id)
          result=true
        rescue => ex
          result=false
          mwlog.info(Time.new.to_s+" Checking request status got exception: "+ex.to_s)
          puts "Checking request status got exception: "+ex.to_s
        end
      else
        result=true
      end

      puts "Checking request status: "+ request_id.to_s + " result: " + result.to_s + " status:" + server_status.to_s
      mwlog.info(Time.new.to_s+" Checking request status: "+ request_id.to_s + " result: " + result.to_s + " status:" + server_status.to_s)
      result
    end

    def mws_submit(mws)
      product=self
      add_product(product,mws)
    end

    def mws_submit_price(mws)
      product=self

      if mws_check_request_status mws
        price=product.price.to_s
        submission = mws.feeds.prices.update(
            Mws::PriceListing(product.sku, price)
        )
        prod=Spree::Product.where(:id => product.id).first
        prod.update_attributes(:mws_status => :submitted_price ,:mws_request_id => submission.id )
        #product.mws_request_id=submission.id
        #product.mws_status=:submitted_price
        #product.save
        puts "price submit! "+product.name
        mwlog.info(Time.new.to_s+" price submit! "+product.name)
      end
    end

    def mws_submit_inventory(mws)
      product=self
      puts "inventory submit! "+product.name
      mwlog.info(Time.new.to_s+" inventory submit! "+product.name)
      if mws_check_request_status mws
        inventory=product.master.stock_items.first.count_on_hand
        if inventory > 0
          submission = mws.feeds.inventory.update(
              Mws::Inventory(product.sku, quantity: inventory, fulfillment_type: :mfn)
          )
          prod=Spree::Product.where(:id => product.id).first
          prod.update_attributes(:mws_status => :submitted_inventory ,:mws_request_id => submission.id )
          #product.mws_request_id=submission.id
          #product.mws_status=:submitted_inventory
          #product.save
          puts "inventory submit! "+product.name
          mwlog.info(Time.new.to_s+" inventory submit! "+product.name)
        end
      end
    end

    def mws_submit_images(mws)
      product=self
      images=product.images
      if images.count>0 && mws_check_request_status(mws)
        add_images(product,mws)
        puts "uploading images! "+product.name
        mwlog.info(Time.new.to_s+" uploading images! "+product.name)

      end
    end


    private

    def add_product(product,mws)
      #add details as width
      sku=product.sku
      upc=product.bar_code
      name=product.name
      if !product.company.nil?
        company=product.company.name
      end
      desc=product.description

      if !(sku.blank? && upc.blank? && name.blank? && company.blank? && desc.blank?)
        mws_product = Mws::Product(sku) {
          upc upc
          tax_code 'GEN_TAX_CODE'
          name name
          manufacturer company
          description desc
        }
        submission = mws.feeds.products.add(mws_product)
        #product.mws_request_id=submission.id
        prod=Spree::Product.where(:id => product.id).first
        prod.update_attributes(:mws_status => :added ,:mws_request_id => submission.id )
        #product.mws_status=:added
        #product.save
        puts 'saved request from :to_submit?'
        mwlog.info(Time.new.to_s+' saved request from :to_submit? ')
      else
      end
    end
    def add_images(product,mws)
      sku=product.sku
      #up to 8 images
      images=[]
      product.images.each_with_index  do |image, index|

        url="http://" + ALInvestShop::Application::DOMAIN_NAME + image.attachment.url(:original)
        if Rails.env.development?
          url="http://productsfromelsalvador.com/assets/resources/al_investlogo.png"
        end
        if index == 0
          images.push(Mws::ImageListing(sku, url, 'Main'))
        elsif index < 8
          images.push(Mws::ImageListing(sku, url, 'PT' + index.to_s))
        end
      end
      submission = mws.feeds.images.add(*images)

      prod=Spree::Product.where(:id => product.id).first
      prod.update_attributes(:mws_status => :submitted_images ,:mws_request_id => submission.id )
      mwlog.info(Time.new.to_s+" submitted images for product: "+prod.name+" "+ images.length.to_s + " images")
      puts "submitted images for product: "+prod.name+" "+ images.length.to_s + " images"
      #product.mws_request_id=submission.id
      #product.mws_status=:submitted_images
      #product.save
    end

  end
end