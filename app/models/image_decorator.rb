Spree::Image.class_eval do

  before_save :mws_update
  #after_create :mws_update
  def mws_update
    product=Spree::Variant.where(:id => self.viewable_id).first.product
    product.mws_status=:submitted_inventory
    product.mws_request_id=nil
    product.save
  end

  attachment_definitions[:attachment][:styles] = {
      :mini => '48x48>', # thumbs under image
      :small => '100x100>', # images on category view
      :product => '240x240>', # full product image
      :slider => '546x170#',
      :large => '600x600>' # light box image
  }

end