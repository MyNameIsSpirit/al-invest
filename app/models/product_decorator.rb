Spree::Product.class_eval do

  attr_accessible :company_id
  belongs_to :company
  attr_accessible :bar_code
  validate :bar_code_is_valid


  #validates :company, presence: true


  def longest_image_url
    image_ratio=0
    longest_url=""
    self.images.each do |i|
      current_ratio=i.attachment_width/i.attachment_height
      if current_ratio>image_ratio
        image_ratio=current_ratio
        longest_url=i.attachment.url(:slider)
      end
    end
    longest_url
  end


  def categories
    return Spree::Category.joins(:products).where(spree_products: {id: id}).all
  end

  def self.get_random_products(args={})
    args[:count]||=3
    category=args[:category_id]
    category_name=args[:category_name]
    if !category_name.nil?
      category=Spree::Category.where(:name => category_name).first.id;
    end
    display_count=args[:count]
    product_count=category.nil? ? Spree::Product.count : Spree::Category.find(category).products.active.count
    offset=0
    if display_count<product_count-display_count
      offset=rand(product_count-display_count).abs
    end
    if category.nil?
      products= Spree::Product.active.limit(display_count).offset(offset)
    else
      products=Spree::Category.find(category).products.active.limit(display_count).offset(offset)
    end
    return products
  end


  def self.most_ordered(args={})
    args[:count]||=3
    display_count=args[:count]
    #TODO: Probably needs optimization
    orders=Spree::Order.where(created_at: (Time.now.midnight - 15.day).. Time.now.midnight).includes(line_items: [:product] ).all
    results=Hash.new

    orders.each do |o|
      o.line_items.each do |l|
        quantity=l.quantity
        if !l.product.nil?
          productId=l.product.id
          if results[productId].nil?
            results[productId]=quantity
          else
            results[productId]+=quantity
          end
        end
      end
    end



    sorted_result=results.sort_by{|id,count| count}

    products=Array.new

    #Checking if we have enough products
    if display_count > sorted_result.length
      missing_products=display_count-sorted_result.length
      random_products=Spree::Product.get_random_products(:count=>missing_products)
      products.concat(random_products)

    end
    popular_products_ids=sorted_result.map{|p| p[0]}
    popular_products_ids=popular_products_ids.take(display_count-products.length)
    popular_products=Spree::Product.where(:id => popular_products_ids).all
    popular_products.concat(products)
    return popular_products
  end



  def self.get_random_from_category(args={})

    if args[:category_id].nil?
      if args[:category_name].nil?
        category=Spree::Category.get_random
      else
        category=Spree::Category.joins(:products).where(:name => args[:category_name]).first
      end
    else
      category=args[:category_id]
    end
    random_products_from_cat=Spree::Product.get_random_products(:count=>args[:count],:category_id=>category)
    if !random_products_from_cat.nil?
      products=random_products_from_cat.active
    else
      products=Array.new
    end
    return products
  end


  private

  def bar_code_is_valid
    if  !bar_code.blank?
      begin
        if !(bar_code.ean? || bar_code.upc?)
          errors.add(:bar_code, "Invalid ean or upc code.")
        end
      rescue => ex
        errors.add(:bar_code, "Invalid ean or upc code.")
      end
    end
  end
end