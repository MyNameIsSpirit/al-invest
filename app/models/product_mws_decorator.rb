Spree::Product.class_eval do

  include MwsApiProducts::Products
  attr_accessible :mws_registration, :mws_status, :mws_request_id,:mws_update
  #before_save :mws_update
  #after_create :mws_create_product
  def mws_update=(value)
    if value
      self.mws_status=:to_submit
    end
  end

  def mws_status
    read_attribute(:mws_status).to_sym
  end

  def mws_status=(new_status)
    write_attribute :mws_status, new_status.to_s
  end
end