Spree::Promotion.class_eval do
  def self.get_random_promotions(args={})
    args[:count]||=3
    display_count=args[:count]
    promotion_count= Spree::Promotion.active.count
    offset=0
    if display_count<promotion_count-display_count
      offset=rand(promotion_count-display_count).abs
    end

    promotions= Spree::Promotion.active.limit(display_count).offset(offset)

    return promotions
  end
end