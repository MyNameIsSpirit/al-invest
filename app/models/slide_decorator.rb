Spree::Slide.class_eval do
  translates :body, :name
  attr_accessible :translations_attributes, :image_english
  accepts_nested_attributes_for :translations

  has_attached_file :image_english
end