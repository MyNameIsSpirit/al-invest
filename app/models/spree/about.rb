class Spree::About < ActiveRecord::Base
  attr_accessible :description, :title
  translates :description, :title
  attr_accessible :translations_attributes
  accepts_nested_attributes_for :translations
end
