module Spree
  class Category < Spree::Taxon
    default_scope {where("parent_id=(Select id from spree_taxons st where st.name like 'Categories')") }

    def self.categories_taxon
      return Spree::Taxon.find_by_sql("Select * from spree_taxons st where st.name like 'Categories'").first
    end

    def self.get_random(args={})
      args[:include_empty]||=false

      if args[:include_empty]
        category_count=Spree::Category.count
        category=Spree::Category.limit(1).offset(rand(category_count-1)).first
      else
        category_count=Spree::Category.joins(:products).count
        category=Spree::Category.joins(:products).limit(1).offset(rand(category_count-1)).first
      end
      return category
    end
  end
end