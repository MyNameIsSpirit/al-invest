class Spree::Company < ActiveRecord::Base
  attr_accessible :history, :name, :photo
  translates :history, :name
  attr_accessible :translations_attributes
  accepts_nested_attributes_for :translations
  validates :history, :name, presence: true
  has_many :products


  has_attached_file :photo,
                    styles: { mini: '48x48>', small: '100x100>',long: '118x152#', product: '240x240>', large: '600x600>' },
                    default_style: :product,
                    url: '/spree/companies/:id/:style/:basename.:extension',
                    #path: ':rails_root/public/spree/products/:id/:style/:basename.:extension',
                    convert_options: { all: '-strip -auto-orient' }

end
