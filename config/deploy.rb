## extra dependencies
require "bundler/capistrano"

#Whenever cron task for mws

require "whenever/capistrano"
set :whenever_command, "bundle exec whenever"
#load "deploy/assets"

## conf
set :application, "ALInvestShop"
set :repository,  "git@bitbucket.org:MyNameIsSpirit/al-invest.git"
set :branch, "production"

 set :scm, :git # You can set :scm explicitly or Capistrano will make an intelligent guess based on known version control directory names
# Or: `accurev`, `bzr`, `cvs`, `darcs`, `git`, `mercurial`, `perforce`, `subversion` or `none`

server = "54.218.103.246"
role :web, server                          # Your HTTP server, Apache/etc
role :app, server                          # This may be the same as your `Web` server
role :db,  server, :primary => true # This is where Rails migrations will run

set :user, "spree"
set :deploy_to, "/home/spree/#{application}"
set :use_sudo, false

default_run_options[:shell] = '/bin/bash --login'
default_environment["RAILS_ENV"] = 'production'

### copying to recently deployed project shared database.yml configuration

task :symlink_database_yml do
  run "rm #{release_path}/config/database.yml"
  run "ln -sfn #{shared_path}/config/database.yml #{release_path}/config/database.yml"
end


after "bundle:install", "symlink_database_yml"


## Unicorn server tasks, zero downtime deployment

namespace :unicorn do
  desc "Zero-downtime restart of Unicorn"
  task :restart, except: { no_release: true } do
      run "kill -s USR2 `cat /tmp/unicorn.ALInvestShop.pid`"
      #run "cd #{current_path}"
      #run "kill -s QUIT `cat /tmp/unicorn.ALInvestShop.pid`"
      #run "cd #{current_path} ; rm -R tmp"
      #run "rm /tmp/ALInvestShop.socket"
      #run "rm /tmp/unicorn.ALInvestShop.pid"
      #run "cd #{current_path} ; bundle exec unicorn_rails -c config/unicorn.rb -D"
    
  end

  desc "Start unicorn"
  task :start, except: { no_release: true } do
    run "cd #{current_path} ; bundle exec unicorn_rails -c config/unicorn.rb -D"
  end

  desc "Stop unicorn"
  task :stop, except: { no_release: true } do
    if File.exist?("/tmp/unicorn.ALInvestShop.pid")
      run "kill -s QUIT `cat /tmp/unicorn.ALInvestShop.pid`"
    end
  end
end

after "deploy:restart", "unicorn:restart"


###Local asset precompiling and upload
before 'deploy:finalize_update', 'deploy:assets:symlink'
after 'deploy:update_code', 'deploy:assets:precompile'

namespace :deploy do
  namespace :assets do

    task :precompile, :roles => :web do
      ##If first time deploy there is no revision, so nothing to compare
      ##We precompile
      begin
        from = source.next_revision(current_revision) # <-- Fail here at first-time deploy
      rescue
        err_no = true
      end
      if err_no ||  capture("cd #{latest_release} && #{source.local.log(from)} vendor/assets/ lib/assets/ app/assets/ app/views/ | wc -l").to_i > 0
        run_locally("rake assets:clean && rake assets:precompile")
        run_locally "cd public && tar -jcf assets.tar.bz2 assets"
        top.upload "public/assets.tar.bz2", "#{shared_path}", :via => :scp
        run "cd #{shared_path} && tar -jxf assets.tar.bz2 && rm assets.tar.bz2"
        run_locally "rm public/assets.tar.bz2"
        run_locally("rake assets:clean")
      else
        logger.info "Skipping asset precompilation because there were no asset changes"
      end
    end

    task :symlink, roles: :web do
      run ("rm -rf #{latest_release}/public/assets &&
            mkdir -p #{latest_release}/public &&
            mkdir -p #{shared_path}/assets &&
            ln -s #{shared_path}/assets #{latest_release}/public/assets")
    end
  end
end

###Images task
namespace :images do
  task :symlink, :except => { :no_release => true } do
    run "rm -rf #{release_path}/public/spree"
    run "ln -nfs #{shared_path}/spree #{release_path}/public/spree"
  end
end
after "bundle:install", "images:symlink"

### nothing to see down here



# if you want to clean up old releases on each deploy uncomment this:
# after "deploy:restart", "deploy:cleanup"

# if you're still using the script/reaper helper you will need
# these http://github.com/rails/irs_process_scripts

# If you are using Passenger mod_rails uncomment this:
# namespace :deploy do
#   task :start do ; end
#   task :stop do ; end
#   task :restart, :roles => :app, :except => { :no_release => true } do
#     run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
#   end
# end
