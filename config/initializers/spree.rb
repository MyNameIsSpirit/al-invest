# Configure Spree Preferences
#
# Note: Initializing preferences available within the Admin will overwrite any changes that were made through the user interface when you restart.
#       If you would like users to be able to update a setting with the Admin it should NOT be set here.
#
# In order to initialize a setting do:
# config.setting_name = 'new value'
Spree.config do |config|
  # Example:
  # Uncomment to override the default site name.
  config.site_name = "Products from El Salvador"
  config.site_url = 'productsfromelsalvador.com'
  SpreeI18n::Config.available_locales=[:en,:es]
  config.admin_interface_logo = 'resources/coexport_logo.png'
end

Spree.user_class = "Spree::User"

#Spree::Config[:allow_ssl_in_production]=false