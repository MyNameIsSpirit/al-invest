# This migration comes from spree_slider (originally 20120222184238)
class CreateSlides < ActiveRecord::Migration
  def self.up
    create_table :spree_slides do |t|
      t.string :link_url
      t.boolean :published
      t.string :image_file_name
      t.string :image_content_type
      t.integer :image_file_size
      t.datetime :image_updated_at
      t.string :name
      t.string :text

      t.timestamps
    end
    Spree::Slide.create_translation_table! :name => :string, :body => :text
  end
  def self.down
    drop_table :spree_slides
    Spree::Slide.drop_translation_table!
  end
end
