class CreateSpreeCompanies < ActiveRecord::Migration
  def self.up
    create_table :spree_companies do |t|
      t.string :photo_file_name
      t.string :photo_content_type
      t.string :photo_file_size
      t.string :name
      t.text :history
      t.timestamps
    end
    Spree::Company.create_translation_table! :name => :string, :history => :text
  end
  def self.down
    drop_table :spree_companies
    Spree::Company.drop_translation_table!
  end
end
