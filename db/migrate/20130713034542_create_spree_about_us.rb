class CreateSpreeAboutUs < ActiveRecord::Migration
  def change
    create_table :spree_about_us do |t|
      t.string :title
      t.string :description

      t.timestamps
    end
  end
end
