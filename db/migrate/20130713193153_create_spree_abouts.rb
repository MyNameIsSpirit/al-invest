class CreateSpreeAbouts < ActiveRecord::Migration
  def self.up
      create_table :spree_abouts do |t|
        t.string :title
        t.string :description

        t.timestamps
        end
      Spree::About.create_translation_table! :title => :string, :description => :text
    end
  def self.down
    drop_table :spree_companies
    Spree::About.drop_translation_table!
  end
end
