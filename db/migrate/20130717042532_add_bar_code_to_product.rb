class AddBarCodeToProduct < ActiveRecord::Migration
  def change
    add_column :spree_products, :bar_code, :string
  end
end
