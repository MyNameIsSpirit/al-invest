class AddAmazonStatusToProduct < ActiveRecord::Migration
  def change
    add_column :spree_products, :mws_status, :string
    add_column :spree_products, :mws_registration, :boolean
    add_column :spree_products, :mws_request_id, :integer
  end
end
