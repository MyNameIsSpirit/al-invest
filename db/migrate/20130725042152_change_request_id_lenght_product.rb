class ChangeRequestIdLenghtProduct < ActiveRecord::Migration
  def up
    change_column :spree_products, :mws_request_id, :bigint
  end

  def down
    change_column :spree_products, :mws_request_id, :int
  end
end
