class AddAttachmentImageEnglishToSlider < ActiveRecord::Migration
  def change
    change_table :spree_slides do |t|
      t.string :image_english_file_name
      t.string :image_english_content_type
      t.integer :image_english_file_size
      t.datetime :image_english_updated_at
    end
  end

end
