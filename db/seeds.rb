# encoding: UTF-8
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


Spree::Core::Engine.load_seed if defined?(Spree::Core)
Spree::Auth::Engine.load_seed if defined?(Spree::Auth)


#Creating base categories

category=Spree::Taxonomy.new(:name=>"Categories")
category.translations.push(Spree::Taxonomy::Translation.new(:locale =>"es",:name => "Categorias"))
category.save
cat=Spree::Taxonomy.where(:name => "Categories").first.taxons.first
cat.translations.push(Spree::Taxon::Translation.new(:locale=>"es",:name=>"Categorias",:permalink =>"categorias"))


t=Spree::Taxon.new(:name=>"Clothing and Accessories",:taxonomy_id=>cat.taxonomy_id,:parent_id=>cat.id,:position=>1)
t.translations.push(Spree::Taxon::Translation.new(:locale =>"es",:name => "Ropa y Accesorios",:permalink => "categorias/ropa-y-accesorios"))
t.save

t=Spree::Taxon.new(:name=>"Crafts",:taxonomy_id=>cat.taxonomy_id,:parent_id=>cat.id,:position=>2)
t.translations.push(Spree::Taxon::Translation.new(:locale =>"es",:name => "Artesanías",:permalink => "categorias/artesanias"))
t.save

t=Spree::Taxon.new(:name=>"Beauty and Health care",:taxonomy_id=>cat.taxonomy_id,:parent_id=>cat.id,:position=>3)
t.translations.push(Spree::Taxon::Translation.new(:locale =>"es",:name => "Belleza y cuidado de la salud",:permalink => "categorias/belleza-y-cuidado"))
t.save

t=Spree::Taxon.new(:name=>"Foodstuff",:taxonomy_id=>cat.taxonomy_id,:parent_id=>cat.id,:position=>4)
t.translations.push(Spree::Taxon::Translation.new(:locale =>"es",:name => "Productos alimenticios",:permalink => "categorias/productos-alimenticios"))
t.save

t=Spree::Taxon.new(:name=>"Ceramic",:taxonomy_id=>cat.taxonomy_id,:parent_id=>cat.id,:position=>5)
t.translations.push(Spree::Taxon::Translation.new(:locale =>"es",:name => "Cerámica",:permalink => "categorias/ceramica"))
t.save

#Creating about entry
a=Spree::About.new(:description => "About us information", :title => "About us title")
a.translations.push(Spree::About::Translation.new(:locale =>"es",:description => "Informacion acerca de",:title => "Titulo acerca de"))
a.save
